all: look_and_say_seq

look_and_say_seq: look_and_say_seq.c
	gcc -o look_and_say_seq -O2 -Wall -W -ansi -pedantic -std=gnu99 look_and_say_seq.c

doc:
	doxygen Doxyfile

format:
	clang-format -i --style=file *.c

clean:
	rm -rf look_and_say_seq html/
